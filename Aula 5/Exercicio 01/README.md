# Exercício 01 #

Implemente um programa que faça o seguinte:

Deve ser criado um vetor de **10** posições inicialmente vazio

O usuário deverá entrar com **10** *prompts* com a descrição:

    x - entre com um valor:

**Obs.:** `x` representa uma numeração que vai de **1** a **10**

Os valores preenchidos no prompt são armazenados no vetor

Os valores do vetor são impressos em uma tabela na ordem inversa em que foram inseridos