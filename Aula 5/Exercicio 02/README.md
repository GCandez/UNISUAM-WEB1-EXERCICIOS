# Exercício 02 #

Implemente um programa que faça o seguinte:

Pergunta ao usuário o tamanho do vetor

Pergunta ao usuário um valor numérico para cada índice do vetor

Exibe a soma dos elementos do vetor com a mensagem:

    O somatório dos x elementos do vetor é: y

Onde: **x** é o número de elementos e **y** é a soma.