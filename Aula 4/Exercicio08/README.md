# Exercício 08 #

Crie um programa em JavaScript onde o usuário entre com uma string através de um prompt, e a mesma string é a apresentada verticalmente em um alerta.

Exemplo: Marcelo vira
M
A
R
C
E
L
O

Para quebrar linha em um alerta use "\n"